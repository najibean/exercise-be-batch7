class Student {
   constructor(name, age, dateBirth, gender, id, hobbies) {
      this._name = name;
      this._age = age;
      this._dateBirth = dateBirth;
      this._gender = gender;
      this.nomorID = id;
      this.hobbies = hobbies || [];
   }

   get name() {
      return this._name;
   }
   set setName(name) {
      this._name = name;
   }

   get age() {
      return this._age;
   }
   set setAge(age) {
      this._age = age;
   }

   get dateBirth() {
      return this._dateBirth;
   }
   set setDateBirth(dateBirth) {
      this._dateBirth = dateBirth;
   }

   get gender() {
      return this._gender;
   }
   set setGender(gender) {
      if(gender === 'male' || gender === 'female'){
         this._gender = gender;
      } else {
         this._gender = 'please input male or female!';
      }
   }

   addHobby(hobiBaru) {
      this.hobbies.push(hobiBaru);
   }

   removeHobby(hobbies) {
      // let index = this.hobbies.indexOf(hobbies);
      // this.hobbies.splice(index, 1);
      // return this.hobbies;

      // -------- 2 cara (yang atas || yang bawah) --------

      let tmp = this.hobbies;
      for(let i=0; i<tmp.length;i++){
         if(tmp[i] == hobbies){
            tmp.splice(tmp.indexOf(hobbies), 1);
         }
      }
      return tmp;
   }

   get getData() {
      return `
      Name: ${this._name}
      Age: ${this._age}
      Date of birth: ${this._dateBirth}
      Gender: ${this._gender}
      ID: ${this.nomorID}
      Hobbies: ${this.hobbies}
      `
   }
}

const dataMurid = new Student('Najib', 30, '13 October 1989', 'male or female?', '0072020015');
dataMurid.setName = 'Najib';
dataMurid.setAge = 30;
dataMurid.setDateBirth = '13 Oktober 1989';
dataMurid.setGender = 'male';

dataMurid.addHobby('makan');
dataMurid.addHobby('minum');
dataMurid.addHobby('musik');

dataMurid.removeHobby('musik');
dataMurid.removeHobby('makan');

console.log(dataMurid.getData);

