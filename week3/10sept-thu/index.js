const Barrack = require('./barrack.js');
const { Knight, Spearman, Archer } = require('./army.js');    // destructuring


const pasukan1a = new Knight('Reihan', 'Kampoeng Durian Runtuh');
const pasukan1b = new Knight('Moesa', 'Kampoeng Nangka');

const pasukan2a = new Spearman('Sulaiman', 17);
const pasukan2b = new Spearman('Harits', 19);

const pasukan3a = new Archer('Najib', 'Batam');
const pasukan3b = new Archer('Daniel', 'Semarang');

const barrack = new Barrack();


// console.log(pasukan1)
// console.log(pasukan2)
// console.log(pasukan3)

barrack.recruit(pasukan1a);
barrack.recruit(pasukan1b);
barrack.recruit(pasukan2a);
barrack.recruit(pasukan2b);
barrack.recruit(pasukan3a);
barrack.recruit(pasukan3b);


barrack.disband('Reihan')

barrack.summon();

barrack.grouping();



