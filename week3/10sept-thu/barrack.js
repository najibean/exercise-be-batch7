class Barrack {
   constructor(slots) {
      this._slots = slots || [];
   }

   recruit(army) {
      this._slots.push(army)
   }
   summon() {
      console.log(this._slots)
   }
   disband(name) {
      for (let i = 0; i < this._slots.length; i++) {
         if (this._slots[i].name === name) {
            this._slots.splice(i, 1)
         }
      }
   }

   grouping() {
      let groupKnight = [];
      let groupSpearman = [];
      let groupArcher = [];
      this._slots.forEach(el => {
         // if(el.type === 'Knight'){
         //    knight.push(el)
         // }

         switch (el.type) {
            case 'Knight':
               groupKnight.push(el);
               break;
            case 'Spearman':
               groupSpearman.push(el);
               break;
            case 'Archer':
               groupArcher.push(el);
               break;
            default:
               console.log('not one of the Armies');
               break;
         };
      });
      let obj = {
         knight: groupKnight,
         spearman: groupSpearman,
         archer: groupArcher
      }
      console.log(obj);
   }
}

module.exports = Barrack;
