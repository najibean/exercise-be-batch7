class Army {
   constructor(name, type, level, latihan) {
      this._name = name;
      this._type = type;
      this._level = level || 1;
      this._latihan = latihan || 0;

   }

   get name() { return this._name }
   set setName(name) { this._name = name }

   get type() { return this._type }
   set setType(type) { this._type = type }

   get level() { return this._level }
   set setLevel(level) { this._level = level }

   get latihan() { return this._latihan }
   set setLatihan(latihan) { this._latihan = latihan }

   // === method talk () ===
   talk() {
      let total = this.level + this.latihan;
      let limitLevel = 10;
      if (total > 1 && total <= limitLevel) {
         console.log(`Bonjour! I'm ${this.name} and my level now is ${total}.`)
      } else if (total > 10) {
         console.log(`Bonjour! I'm ${this.name} and my level is already ${total}.`)
      } else {
         console.log(`Bonjour! I'm ${this.name} and my level is still ${total}.`)
      }
   }

   // === method training () ===
   training() {
      let total = this.level + this.latihan;
      let limitLevel = 10;
      if (total > 1 && total <= limitLevel) {
         console.log(`${this.name} can go to war!`)
      } else if (total > limitLevel) {
         console.log(`Sorry Sir, you can't join this war because we need you at the base...`)
      } else {
         console.log(`${this.name} can't go to war and need to training again!`)
      }
   }
}


class Knight extends Army {
   constructor(name, village) {
      super(name, 'Knight', 0, 0, village);  // inherite from Army
      this._name = name;
      this._village = village;
   }
   get village() { return this._village }
   set setVillage(village) { this._village = village }

   talk() {
      console.log(`${this.name} is come from ${this.village} and one of the Knight Army`);
   }
}


class Spearman extends Army {
   constructor(name, age) {
      super(name, 'Spearman', 0, 0, age);    // inherite from Army
      this._name = name;
      this._age = age;
   }
   get age() { return this._age }
   set setAge(age) { this._age = age }

   talk() {
      console.log(`${this.name} is ${this.age} years old and one of the Spearman Army`);
   }
}


class Archer extends Army {
   constructor(name, city) {
      super(name, 'Archer', 0, 0, city);    // inherite from Army
      this._name = name;
      this._city = city;
   }
   get city() { return this._city }
   set setCity(city) { this._city = city }

   talk() {
      console.log(`${this.name} from ${this.city} and He is one of the Archer Army`);
   }
}
// const pasukan3 = new Knight('Sulaiman', 'Batam'); // diisi yang sesuai constructornya, bukan yang super()


module.exports = { Knight, Spearman, Archer };


