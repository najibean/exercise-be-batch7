const { Router } = require('express');
const router = Router();
const ProductControllers = require('../controllers/product');

// Middlewares
const { authentication, authorization } = require('../middlewares/auth')

router.get('/', authentication, ProductControllers.getProduct);
router.post('/', authentication, ProductControllers.addProduct);
router.delete('/:id', authentication, authorization, ProductControllers.deleteProduct);
router.put('/:id', authentication, authorization, ProductControllers.updateProduct);

module.exports = router;