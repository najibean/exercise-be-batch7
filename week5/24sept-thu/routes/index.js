const { Router } = require('express');
const router = Router();
const userRoutes = require('./user');
const productRoutes = require('./product');

router.get('/', (req, res) => {
   res.status(200).json({
      msg : 'This is home page'
   })
})

router.use('/user', userRoutes);
router.use('/product', productRoutes);


module.exports = router;
