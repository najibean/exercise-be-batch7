const { Router } = require('express');
const router = Router();
const userControllers = require('../controllers/user');

router.get('/', userControllers.list);
router.post('/login', userControllers.login);

router.post('/register', userControllers.register);

module.exports = router;