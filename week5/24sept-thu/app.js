const express = require('express');
const app = express();
require('dotenv').config();

const port = process.env.PORT || 3000;

const errorHandling = require('./middlewares/errorHandling')
const routes = require('./routes/index')

//Middlewares
app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({ extended: false}));

//Routes
app.use(routes);
app.use(errorHandling);     //passing middleware disini, setelah routes


app.listen(port, () => {
    console.log(`Server is running at port : ${port}`);
})
