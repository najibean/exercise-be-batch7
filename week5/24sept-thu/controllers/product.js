const { Product, User } = require('../models')

class ProductController {
   static async getProduct(req, res, next) {
      try {
         const result = await Product.findAll({
            order: [
               ['id', 'ASC']
            ],
            include: [
               User
            ]
         })
         res.status(200).json(result);

      }
      catch (err) {
         // res.status(500).json(err);
         next(err);
      }
   }

   static async addProduct(req, res, next) {
      const { name, info, image, price, stock } = req.body;
      const UserId = req.userData.id
      try {
         const found = await Product.findOne({
            where: {
               name
            }
         })
         if (found) {
            res.status(409).json({
               msg: "Name already exist! Try another name arigato."
            })
         } else {
            const product = await Product.create({
               name, info, image, price, stock, UserId
            })

            res.status(201).json(product)
         }
      } catch (err) {
         // res.status(500).json(err);
         next(err);
      }
   }

   static async deleteProduct(req, res, next) {
      const id = req.params.id;
      try {
         Product.destroy({
            where: {
               id
            }
         })
         res.status(200).json({
            msg: 'Product deleted!'
         })
      } catch (err) {
         // res.status(500).json(err);
         next(err);
      }
   }

   static updateProduct(req, res, next) {
      const { name, info, image, price, stock } = req.body;
      const id = req.params.id;
      Product.update({
         name, info, image, price, stock
      }, {
         where: {
            id
         }
      })
         .then(() => {
            res.status(200).json({
               msg: 'Product updated!'
            });
         })
         .catch(err => {
            // res.status(500).json(err);
            next(err);
         })

   }

   // belum didefinisikan di router
   static async editProduct(req, res) {
      const { name, info, image, price, stock } = req.body;
      const id = req.params.id;
      // const image = req.file.path;
      try {
         const edit = await Product.update({
            name,
            info,
            image,
            price,
            stock
         }, {
            where: { id }
         });
         res.status(203).json(edit);
      } catch (err) {
         res.status(500).json(err);
      }
   }
}

module.exports = ProductController;