const { User } = require('../models');
const { decryptPwd } = require('../helpers/bcrypt');
const { tokenGenerator } = require('../helpers/jwt');


class UserControllers {
   static async list(req, res) {
      try {
         const users = await User.findAll()

         res.status(200).json(users);
      } catch(err) {
         res.status(500).json(err);
      }
   }

   static async login(req, res) {
      const { username, password} = req.body;

      try{
         const userFound = await User.findOne({
            where: {
               username
            }
         })
         if(userFound) {
            const pwdDecrypt = decryptPwd(password,userFound.password);
            if(pwdDecrypt) {
               const access_token = tokenGenerator(userFound);
               res.status(200).json({
                  access_token
               });
               
            } else {
               throw {
                  status: 400,
                  msg: 'Password did not match!'
               }
            }
         } else {
            // bisa juga seperti dibawah ini!
            // res.status(404).json({
            //    msg: 'User is not found'
            // })
            throw {
               msg: 'User is not found'
            }
         }

      } catch (err) {
         res.status(500).json(err);
      }

   }

   static async register(req, res) {
      const { username, password } = req.body;

      try {
         // const pwdEncrypt = bcrypt.hashSync(password, saltRound);
         const user = await User.create({
            username, password
         })
         res.status(201).json(user)

      } catch (err) {
         res.status(500).json(err)
      }
   }
}

module.exports = UserControllers;