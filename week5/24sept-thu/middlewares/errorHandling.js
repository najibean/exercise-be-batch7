module.exports = (err, req, res, next) => {
   let status = 500;
   let msg = 'Internal server error'

   if(err.name === 'SequelizeValidationError') {
      status = 500;
      msg = err.errors[0].message   //karena property errors memuat data array didalamnya
   }
   res.status(status).json({
      status, msg
   });

}