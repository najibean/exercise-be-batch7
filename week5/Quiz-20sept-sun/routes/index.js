const { Router } = require('express');
const router = Router();
const booksRoute = require('./books');

router.get('/', (req,res)=>{
   res.render('index.ejs')
});

router.use('/books', booksRoute)




