const { Router } = require('express');
const router = Router();
const booksController = require('../controllers/books');

router.get('/', booksController.getBook)

// menampilkan halaman form untuk menambahkan data buku
router.get('/books/add', booksController.addFormBook) 

// menerima data yang dikirim dari halaman /books/add untuk melakukan insertion kedalam table Books
router.post('/books/add', booksController.addBook)

// menampilkan halaman form untuk mengedit data buku
router.get('/books/edit/:id', booksController.findById)

// menerima data yang dikirim dari halaman /books/edit untuk melakukan update data buku berdasarkan id yang dikirim
router.post('/books/edit/:id', booksController.updateBook)

// melakukan delete data buku berdasarkan id yang dikirim
router.get('/books/delete/:id', booksController.deleteBook)

module.exports = router;