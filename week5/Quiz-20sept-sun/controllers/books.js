const { Books } = require('../models')

class BooksController {
   static getBook(req, res) {
      Books.findAll()
         .then(result => {
            // res.send(result)
            res.render('book.ejs', { books: result })
         })
         .catch(err => {
            console.log(err);
         })
   }
   static addFormBook(req, res) {
      res.render('addBooks.ejs');
   }
   static addBook(req, res) {
      const { title, author, released_date, page, genre } = req.body;
      Books.create({
         title,
         author,
         released_date,
         page,
         genre
      })
         .then(result => {
            // res.send(result)
            res.redirect('/books')
         })
         .catch(err => {
            res.send(err)
         })
   }

   static findById(req, res) {
      const id = req.params.id;
      Books.findOne({
         where: { id }
      })
         .then(result => {
            res.send(result)
         })
         .catch(err => {
            res.send(err)
         })
   }

   static deleteBook(req, res) {
      const id = req.params.id;
      Books.destroy({
         where: { id }
      })
         .then(() => {
            // res.send("Deleted")
            res.redirect('/books')
         })
         .catch(err => {
            res.send(err)
         })
   }

   static updateBook(req, res) {
      const id = req.params.id;
      const { title, author, released_date, page, genre } = req.body;
      Books.update({
         title,
         author,
         released_date,
         page,
         genre
      }, {
         where: { id }
      })
         .then(result => {
            res.send(result)
         })
         .catch(err => {
            res.send(err)
         })
   }
}

module.exports = BooksController; 