const express = require('express');
const app = express();
const port = 3010;

const routes = require('./routes/index')

//Middlewares
app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({ extended: false}));

//Routes
app.use(routes);

app.listen(port, () => {
    console.log(`Server is running at port : ${port}`);
})
