const { Ships } = require('../models')

class ShipsController {
   static getShip(req, res) {
      Ships.findAll()
         .then(result => {
            // res.send('coba')
            res.render('Ships.ejs', {ships: result})
         })
         .catch(err => {
            console.log(err)
         })
   }
   static addFormShip(req, res) {
      res.render('addShips.ejs');
   }
   static addShip(req, res) {
      const { name, type, power } = req.body;
      Ships.create({
         name,
         type,
         power,
      })
         .then(result => {
            res.send(result)
            // res.redirect('/Ships')  // langsung menuju halaman ./Ships
         })
         .catch(err => {
            res.send(err)
         })
   }
   static findById(req, res) {
      const id = req.params.id;
      Ships.findOne({
         where: { id }
      })
         .then(result => {
            res.send(result)
         })
         .catch(err => {
            res.send(err)
         })
   }

   static deleteShips(req, res) {
      const id = req.params.id;
      Ships.destroy({
         where: { id }
      })
         .then(() => {
            // res.send("Deleted")
            res.redirect('/Ships')
         })
         .catch(err => {
            res.send(err)
         })
   }
}

module.exports = ShipsController;
