const { Pirates } = require('../models')

class PiratesController {
   static getPirate(req, res) {
      Pirates.findAll()
         .then(result => {
            // res.send('coba getPirate')
            res.render('Pirates.ejs', {pirates: result})
         })
         .catch(err => {
            console.log(err)
         })
   }
   static addFormPirate(req, res) {
      res.render('addPirates.ejs');
   }
   static addPirate(req, res) {
      const { name, type, power } = req.body;
      Pirates.create({
         name,
         type,
         power,
      })
         .then(result => {
            res.send(result)
            // res.redirect('/Pirates')  // langsung menuju halaman ./Pirates
         })
         .catch(err => {
            res.send(err)
         })
   }
   static findById(req, res) {
      const id = req.params.id;
      Pirates.findOne({
         where: { id }
      })
         .then(result => {
            res.send(result)
         })
         .catch(err => {
            res.send(err)
         })
   }

   static deletePirates(req, res) {
      const id = req.params.id;
      Pirates.destroy({
         where: { id }
      })
         .then(() => {
            // res.send("Deleted")
            res.redirect('/Pirates')
         })
         .catch(err => {
            res.send(err)
         })
   }
}

module.exports = PiratesController;
