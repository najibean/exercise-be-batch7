const { Router } = require('express');
const router = Router();
const PiratesController = require('../controllers/Pirates');

router.get('/pirates', PiratesController.getPirate)
router.get('/pirates/add', PiratesController.addFormPirate)
router.post('/pirates/add', PiratesController.addPirate)
router.delete('/pirates/delete/:id', PiratesController.deletePirates)

module.exports = router;
