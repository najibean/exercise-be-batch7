const { Router } = require('express');
const router = Router();
const ShipsController = require('../controllers/Ships');

router.get('/ships', ShipsController.getShip)
router.get('/ships/add', ShipsController.addFormShip)
router.post('/ships/add', ShipsController.addShip)
router.delete('/ships/delete/:id', ShipsController.deleteShips)

module.exports = router;
