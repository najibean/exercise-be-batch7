'use strict';
const fs = require('fs');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const parseData = JSON.parse(fs.readFileSync('./Pirates.json'));
    const PiratesData = [];
    parseData.forEach(data => {
      const { name, status, haki } = data;
      PiratesData.push({
        name,
        status,
        haki,
        createdAt : new Date(),
        updatedAt : new Date(),
      })
    })
    await queryInterface.bulkInsert('Pirates', PiratesData, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Pirates', null, {});
  }
};
