class View {
   static list(data) {
      for (let i = 0; i < data.length; i++) {
         if(data[i].status === true) {
            console.log(`[X] ${data[i].task}`);
         } else {
            console.log(`[ ] ${data[i].task}`);
         }
      }
   }
   
   static message(data) {
      console.log(data);
   }

   static help() {
      // menu help dipindah disini!
      console.log(`
         node todo.js  
         node todo.js help
         node todo.js list  
         node todo.js add <task>
         node todo.js update <id> <task>
         node todo.js delete <id>
         node todo.js complete <id>
         node todo.js uncomplete <id>
      `);
   }
}

module.exports = View;