const fs = require('fs');

class TodoModel {
   constructor(id, task, status, tag, created_at, completed_at) {
      this.id = id;
      this.task = task;
      this.status = status;
      this.tag = tag;
      this.created_at = created_at;
      this.completed_at = completed_at;  
   }

   static list() {
      const data = fs.readFileSync('./data.json', 'utf8');  
      const parseData = JSON.parse(data);

      let tempArr = [];
      parseData.forEach(el => {
         const { id, task, status, tag, created_at, completed_at } = el;   // destructuring
         tempArr.push(new TodoModel(id, task, status, tag, created_at, completed_at))
      });

      return tempArr;
   }

   // === menambahkan TODO kedalam list ===
   static add(params) {
      // destructuring params
      const [task, status] = params;

      const tempData = this.list();
      const idGenerate = tempData[tempData.length - 1].id + 1;

      const tempObject = {
         id: idGenerate,
         task: task,
         status: (status === 'true'),
         tag: [],
         created_at: new Date(),
         completed_at: status === 'true' ? new Date() : null
      }
      tempData.push(tempObject);

      this.save(tempData);
      return `"${task}" has been created!`;
   }

   // === melihat detail TODO sesuai id nya ===
   static update(params) {
      const tempTodo = this.list();
      const idTodo = Number(params[0]);
      const task = params[1]

      
      for (let i = 0; i < tempTodo.length; i++) {
         const taskYangDiganti = tempTodo[i].task;
         if(tempTodo[i].id === idTodo) {
            tempTodo[i].task = task;
            this.save(tempTodo);
            return `"${taskYangDiganti}" has been changed to "${task}"`;
         } 
      }
      
      for (let i = 0; i < tempTodo.length; i++) {
         if(tempTodo[i].id !== idTodo) {
            return `nomor ID is not in database!`;
         }
      }
   
   }

   // === menghapus TODO sesuai dengan "id" nya ===
   static delete(params) {
      const tempTodo = this.list();
      const idTodo = Number(params[0]);

      const tempData = tempTodo.filter(el => el.id !== idTodo);

      for (let i = 0; i < tempTodo.length; i++) {
         if(tempTodo[i].id === idTodo) {
            this.save(tempData);
            return `"${tempTodo[i].task}" has been remove!`;
         } 
      }
      
      for (let i = 0; i < tempTodo.length; i++) {
         if(tempTodo[i].id !== idTodo) {
            return `nomor ID is not in database!`;
         } 
      }

   }
   
   static complete(params) {
      const tempTodo = this.list();
      const idTodo = Number(params[0]);

      for (let i = 0; i < tempTodo.length; i++) {
         if(tempTodo[i].id === idTodo) {
            if(tempTodo[i].status === false) {
               tempTodo[i].status = true;
               tempTodo[i].completed_at = new Date();
               this.save(tempTodo);
            }
            return `"${tempTodo[i].task}" has been completed`;
         } 
      }
      
      for (let i = 0; i < tempTodo.length; i++) {
         if(tempTodo[i].id !== idTodo) {
            return `nomor ID is not in database!`;
         }
      }
   }

   static uncomplete(params) {
      const tempTodo = this.list();
      const idTodo = Number(params[0]);

      for (let i = 0; i < tempTodo.length; i++) {
         if(tempTodo[i].id === idTodo) {
            if(tempTodo[i].status === true) {
               tempTodo[i].status = false;
               tempTodo[i].completed_at = null;
               this.save(tempTodo);
            }
            return `"${tempTodo[i].task}" has been uncompleted`;
         } 
      }
      
      for (let i = 0; i < tempTodo.length; i++) {
         if(tempTodo[i].id !== idTodo) {
            return `nomor ID is not in database!`;
         }
      }
   }

   static save(data) {
      fs.writeFileSync('./data.json', JSON.stringify(data, null, 2));
   }
}


module.exports = TodoModel;




