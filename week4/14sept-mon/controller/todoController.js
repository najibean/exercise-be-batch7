const todoModel = require('../model/todoModel');
const view = require('../view/todoView');


// semua method menggunakan static!
class TodoController {
   static help(){
      view.help();
   }
   static list() {
      const list = todoModel.list();  
      view.list(list);
   }
   static add(params) {
      const data = todoModel.add(params);
      view.message(data);
   }
   static update(params) {
      const data = todoModel.update(params);
      view.message(data);
   }
   static delete(params) {
      const data = todoModel.delete(params);
      view.message(data);
   }
   static complete(params) {
      const data = todoModel.complete(params);
      view.message(data);
   }
   static uncomplete(params) {
      const data = todoModel.uncomplete(params);
      view.message(data);
   }
   static message() {
      view.message('masukkan perintah yang benar');
   }
}

module.exports = TodoController;