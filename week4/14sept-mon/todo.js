const command = process.argv[2]; //dimulai dari index ke-2
const params = process.argv.slice(3);  //array baru dimulai dari index ke-3 dan seterusnya
const todoController = require('./controller/todoController'); //apakah disertakan .js-nya atau tidak?

switch(command) {
   case 'help' :
      todoController.help();
      break;
   case 'list' :
      todoController.list(); 
      break;
   case 'add' :
      todoController.add(params);
      break;
   case 'update' :
      todoController.update(params);
      break;
   case 'delete' :
      todoController.delete(params);
      break;
   case 'complete' :
      todoController.complete(params);
      break;
   case 'uncomplete' :
      todoController.uncomplete(params);
      break;
   default :
      todoController.message();  //menampilkan pesan jika input bukan dari semua yang diatas.
      break;
}

