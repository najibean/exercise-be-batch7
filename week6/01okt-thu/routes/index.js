const router = require('express').Router();
const teacherRoutes = require('./teacher');
const studentRoutes = require('./student');
// const feedbackRoutes = require('./feedback');
// const profileRoutes = require('./profile');
// const employerRoutes = require('./employer');


router.get('/', (req, res) => {
   res.status(200).json({
      msg : 'This is home page'
   })
})

// Routes
router.use('/teachers', teacherRoutes);
router.use('/students', studentRoutes);
// router.use('/feedbacks', feedbackRoutes);
// router.use('/profiles', profileRoutes);
// router.use('/employers', employerRoutes);


module.exports = router;
