const router = require('express').Router();
const studentController = require('../controllers/student')

router.get('/', studentController.list)
router.post('/register', studentController.register)
router.post('/login', studentController.login)
router.delete('/delete/id', studentController.deleteStudent)


module.exports = router;