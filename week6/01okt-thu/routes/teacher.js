const router = require('express').Router();
const teacherController = require('../controllers/teacher')

router.get('/', teacherController.getTeacher)
router.post('/add', teacherController.addTeacher)
router.delete('/delete/id', teacherController.deleteTeacher)
// router.post('/login', teacherController.login)


module.exports = router;