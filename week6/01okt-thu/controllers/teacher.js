const { Teacher, Employer } = require('../models')

class TeacherController {
    static async getTeacher(req, res, next) {
        try {
            const result = await Teacher.findAll({
                order: [
                    ['id', 'ASC']
                ],
                // include: [
                //     Employer
                // ]
            })
            res.status(200).json({
                result,
                msg: 'anda sudah masuk endpoint teachers'
            });

        }
        catch (err) {
            res.status(500).json(err);
            // next(err);
        }
    }

    static async addTeacher(req, res, next) {
        const { name, score, subject, image, employerId } = req.body;
        // const employerId = req.employerData.id
        try {
            const found = await Teacher.findOne({
                where: {
                    name
                }
            })
            if (found) {
                res.status(409).json({
                    msg: "Name already exist! Try another name arigato."
                })
            } else {
                const teacher = await Teacher.create({
                    name, score, subject, image, employerId
                })

                res.status(201).json(teacher)
            }
        } catch (err) {
            // res.status(500).json(err);
            next(err);
        }
    }

    static async deleteTeacher(req, res, next) {
        const id = req.params.id;
        try {
            Teacher.destroy({
                where: {
                    id
                }
            })
            res.status(200).json({
                msg: 'Teacher deleted!'
            })
        } catch (err) {
            // res.status(500).json(err);
            next(err);
        }
    }

    static updateTeacher(req, res, next) {
        const { name, info, image, price, stock } = req.body;
        const id = req.params.id;
        Teacher.update({
            name, info, image, price, stock
        }, {
            where: {
                id
            }
        })
            .then(() => {
                res.status(200).json({
                    msg: 'Teacher updated!'
                });
            })
            .catch(err => {
                // res.status(500).json(err);
                next(err);
            })


        // gak tau nih gimana kalau pakai async-await nya
        // try {
        //    Teacher.update({
        //       where: {
        //          id
        //       }
        //    })
        //    res.status(200).json({
        //       msg: 'Teacher updated!'
        //    })

        // } catch (err) {
        //    res.status(500).json(err);
        // }
    }
}

module.exports = TeacherController;