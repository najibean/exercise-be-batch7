const { Student } = require('../models')
const { decryptPwd } = require('../helpers/bcrypt')
const { tokenGenerator } = require('../helpers/jwt')

class StudentController {
   static async list(req, res) {
      try {
         const students = await Student.findAll()
         res.status(200).json(students);
      } catch (err) {
         res.status(500).json(err);
      }
   }

   static async profile(req, res) {
      const id = req.params.id
      try {
         const found = await Student.findOne({
            where: {
               id
            }
         })
         if (found) {
            res.status(200).json(found)
         } else {
            res.status(404).json({ 
               msg: "Student not Found" 
            })
         }
      } catch (err) {
         res.status(500).json(err);
      }
   }

   static async login(req, res) {
      const { username, password } = req.body;
      try {
         const StudentFound = await Student.findOne({
            where: {
               username
            }
         })
         if (StudentFound) {
            if (decryptPwd(password, StudentFound.password)) {
               const access_token = tokenGenerator(StudentFound)
               res.status(200).json({ access_token })
            } else {
               throw {
                  status: 400,
                  msg: "Password is not the same."
               }
            }
         } else {
            res.status(404).json({
               status: 404,
               msg: "Student is not found."
            })
            // throw {
            //   status: 404,
            //   msg: "Student is not found."
            //  }
         }
      } catch (err) {
         res.status(500).json(err)
      }
   }

   static async register(req, res) {
      const { username, password } = req.body;
      try {
         const student = await Student.create({
            username, password
         })
         res.status(201).json(student)
      } catch (err) {
         res.status(500).json(err)
      }
   }

   // static async editStudent(req, res) {
   //    const id = req.params.id;
   //    const { name } = req.body;
   //    // const image = req.file.path;
   //    try {
   //       const edit = await Student.update({
   //          name,
   //          image
   //       }, {
   //          where: { id }
   //       });
   //       res.status(203).json(edit);
   //    } catch (err) {
   //       res.status(500).json(err);
   //    }
   // }

   static async deleteStudent(req, res) {
      const id = req.params.id
      try {
         const result = await Student.destroy({
            where: { id }
         });
         res.status(202).json(result);
      } catch (err) {
         res.status(500).json(err);
      }
   }
}

module.exports = StudentController;