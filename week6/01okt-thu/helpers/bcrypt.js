const bcrypt = require('bcrypt');
const saltRound = Number(process.env.SALT_ROUND);  // env adalah dalam bentuk string, sehingga harus di convert dahulu ke number


const encryptPwd = (password) => {
   return bcrypt.hashSync(password, saltRound);
}

// versi shorthand arrow function
const decryptPwd = (password, userPwd) => bcrypt.compareSync(password, userPwd);


module.exports = {
   encryptPwd, decryptPwd
}