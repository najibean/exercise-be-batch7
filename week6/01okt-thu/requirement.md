# Apps Requirement
Node.js, Express, SQL, ORM, Sequelize, JWT, Bcrypt


### Table Relations :
- each Teacher has many Feedbacks from students
- Students can give 1 Feedback only
- each Student has 1 Profile
- Teachers has an Employer
- Employer can have many Teachers

### Table Information
- Teachers  : name, score, subject, image, employerId
- Students  : username,password
- Profiles  : image, gender, address, StudentId
- Feedbacks : info, score, TeacherId, StudentId (junction)
- Employer  : name

### Use Case
1. Students can give a Feedback in tacher page
2. In teacher's page shows feedbacks
3. Students can edit their profiles in profile Page
4. Home page shows Teachers
5. Students can CRUD their feedbacks


## Features
1. Student API
   - Student can login-register
   - Student will get access_token after login and register (maksudnya setelah register bisa otomatis login)
   - Student can show their Profiles
2. Teacher API
   - Teacher will get their feedbacks in home page
   - CRUD teacher only in Postman
3. Profiles API
   - Profiles will show their list
4. Feedbacks API
   - feedbacks will have score and info
   - feedbacks will show in Teacher's page
5. Employer
   - Employer can show their list


## Answer:
1. Analyze the problems
   - Perusahaan ingin membuat aplikasi feedbacks yang dapat diberikan oleh murid kepada seorang guru
   - Student ingin memberikan feedbacks kepada seorang guru
2. Relationship between table
   - _Teachers -> Students = many to many_
   - Teachers -> Feedbacks = many to one
   - Students -> Feedbacks = many to one
   - Students -> Profile = one to one
   - Employer -> Teachers = one to many
3. draw the ERD
   - https://dbdiagram.io/d/5f7bdc763a78976d7b7675ce