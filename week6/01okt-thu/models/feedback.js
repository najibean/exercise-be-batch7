'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Feedback extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Feedback.init({
    info: { 
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Info must be filled!`
        }
      }
    },
    score: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: {
          msg: `Score must be filled!`
        },
        isNumeric: {
          msg: `Score must be in number!`
        }
      }
    },
    teacherId: DataTypes.INTEGER,
    studentId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Feedback',
  });
  return Feedback;
};