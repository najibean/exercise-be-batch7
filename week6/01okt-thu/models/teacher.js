'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Teacher extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Teacher.belongsToMany(models.Student, {through: 'models.Feedback'})
    }
  };
  Teacher.init({
    name: { 
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Name must be filled!`
        }
      }
    },
    score: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: {
          msg: `Score must be filled!`
        },
        isNumeric: {
          msg: `Score must be in number!`
        }
      }
    }, 
    subject: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Subject must be filled!`
        }
      }
    },
    image: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Image must be filled!`
        }
      }
    },
    employerId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Teacher',
  });
  return Teacher;
};