'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Student extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Student.init({
    username: { 
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Username must be filled!`
        }
      }
    },
    password: { 
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: `Password must be filled!`
        }
      }
    }
  }, {
    sequelize,
    modelName: 'Student',
  });
  return Student;
};