/* ----- 2. Print the n first numbers ----- */

var firstNumber = function (angka) {

   for (let i = 1; i <= angka; i++) {

      if ((i % 3 === 0) && (i % 5 === 0)) {
         console.log('Kelipatan 3 dan 5');
      } else if (i % 5 === 0) {
         console.log('Kelipatan 5');
      } else if (i % 3 === 0) {
         console.log('Kelipatan 3');
      } else {
         console.log(i);
      }
   }
}
firstNumber(15);