/* ----- 4. Split words without function .split(" ") ----- */

// Using For
function wordsSplitFor(str) {
   var character = [];
   var temp = '';

   for (let i = 0; i < str.length; i++) {
      if (str[i] !== ' ') {
         temp += str[i];
      }
      else {
         character.push(temp)
         temp = '';
      }
      if (i === str.length - 1) {
         character.push(temp);
      }
   }
   console.log(character);
}
wordsSplit('Lorem ipsum is dummy text');

// Using While
// code here....