
// ============== cara 1 ============== 
// function sortLetter(str) {
//    let huruf = [];

//    // change from string to element in Array
//    for (let i = 0; i < str.length; i++) {
//       huruf.push(str[i]);
//    }
   
//    // sorting each element in Array
//    for (let j = 0; j < huruf.length; j++) {
//       if (huruf[j] > huruf[j+1]) {
//          let tmp = huruf[j];
//          huruf[j] = huruf[j + 1];
//          huruf[j + 1] = tmp;   
//       }
//    }
//    // use built-in function "join()" to join each element in Array and return string data type
//    console.log(huruf.join(''));
// }

// //Test Case
// sortLetter("hello")



// ============== cara 2 ============== 
// let sortLetter2 = (str) => str.split('').sort().join('');
// console.log(sortLetter2("hello"));



// ============== cara 3 ============== 
const sortLetter = (str) => {
   let huruf = 'abcdefghijklmnopqrstuvwxyz';
   let string = '';

   for (let i = 0; i < huruf.length; i++) {
       for (let j = 0; j < str.length; j++) {
           if (huruf[i] === str[j]) {
               string += str[j];
           }
       }
   }
   return string
}
console.log(sortLetter("hello"));
