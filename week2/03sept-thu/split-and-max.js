
let string = "Lorem ipsum is dummy text";
const maxWordLength = (string) => {
   let temp = [];
   let word = "";

   for (let i = 0; i < string.length; i++) {
      if (string[i] !== " ") {
         word += string[i];
         // console.log(word);
      }
      else {
         // console.log(word)
         temp.push(word);
         word = "";
      }

      if (i === string.length - 1) {
         temp.push(word);
      }
   }
   return temp
}
// console.log(splitWords(string));

const maxWordLength = () => {
   let maxWord = "";
   let nextWord = "";
   string += " "; // to count the last word

   for (let i = 0; i < string.length; i++) {
      
      if (string[i] === " ") {
         if (nextWord.length > maxWord.length) {
            maxWord = nextWord;
         }
         i++;
         nextWord = "";
      }
      nextWord += string[i];
   }
   return maxWord;
}

// Test Case
maxWordLength(string); //Lorem