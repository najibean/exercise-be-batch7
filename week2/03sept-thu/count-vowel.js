
// ============== cara 1 ============== 
// const vowelsObject = (string) => {
//     let temp = {}
//     temp.a = countVowel(string, "a")
//     temp.i = countVowel(string, "i")
//     temp.u = countVowel(string, "u")
//     temp.e = countVowel(string, "e")
//     temp.o = countVowel(string, "o")
//     console.log(temp);
// }

// const countVowel = (string, char) => {
//     let flag = 0;
//     for (let i = 0; i < string.length; i++) {
//         if (string[i] === char) {
//             flag++;
//         }
//     }
//     // string.forEach(el => {
//     //     if(el === char){
//     //         flag ++;
//     //     }
//     // })
//     return flag;
// }
// vowelsObject('rum raisin chocolate ice cream');



// ============== cara 2 ============== 
const vowelsObject = (str) => {
    let a = 0;
    let i = 0;
    let u = 0;
    let e = 0;
    let o = 0;

    let temp = {};
    for (let j = 0; j < str.length; j++) {
        if (str[j] === 'a') {
            a++;
        }
        else if (str[j] === 'i') {
            i++; 
        }
        else if (str[j] === 'u') {
            u++;
        }
        else if (str[j] === 'e') {
            e++;
        }
        else if (str[j] === 'o') {
            o++;
        }
    }
    temp.a = a;
    temp.i = i;
    temp.u = u;
    temp.e = e;
    temp.o = o;
    return temp;
}
console.log(vowelsObject('rum raisin chocolate ice cream'));



// ============== cara 3 ============== 
// const vowelsObject = (str) => {
//     let huruf = 'aiueo';
//     let temp = {
//         a: 0,
//         i: 0,
//         u: 0,
//         e: 0,
//         o: 0
//     };

//     for (let i = 0; i < huruf.length; i++) {
//         for (let j = 0; j < str.length; j++) {
//             if (huruf[i] === str[j]) {
//                 temp[huruf[i]]++;
//             }
//         }
//     }
//     return temp;
// }
// console.log(vowelsObject('rum raisin chocolate ice cream'));


//Test Case
/*
    {
        a : 3,
        i : 3,
        u : 1,
        e : 3,
        o : 2
    }
*/