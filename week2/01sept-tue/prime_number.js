
//code here
/**
* Algoritma
* 
* - definisi bil prima --> bil yang memiliki faktor 1 dan bilangan itu sendiri
*                      --> yang hanya bisa dibagi 1 dan bilangan itu sendiri, dan tidak memiliki sisa bagi (modulus)
* - faktor = 2
* - print pake while
*/


function checkPrime(number) {
   var jumFaktor = 0;
   for (let i = 1; i <= number; i++) {
      if (number % i === 0) {
         jumFaktor++;   //teknik FLAG, jumlah faktor adalah 2
      }
   }
   
   if(jumFaktor === 2){    //setelah dibagi dgn 1 dan bil itu sendiri; 2
      return true;
   } else{
      return false;
   }
   
}
// console.log(checkPrime(5))


function print(n){
   let x = 0;
   let number = 1;

   if(n < 1){
      console.log(1);
   } 
   else {
      while(x < n){
         if(checkPrime(number)){
            console.log(number);
            x++;
         }
         number++;
      }
   }
}

//Test Case
print(3); //2 3 5
// print(5); //2 3 5 7 11 
// print(0); //1