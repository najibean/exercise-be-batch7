/** 
 * 
 * Algoritma:
 * + definisikan apa itu FPB ==> adalah bil yang terbesar dari bil-bil input yang hanya bisa dibagi dengan dirinya sendiri hingga habis (tak bersisa)
 * + dari 2 bilangan, tentukan yang terkecil dengan IF
 * + menyisir seluruh angka dalam bilangan tersebut dengan FOR loop, dengan exe IF angka dalam bil dimodulus index-loop = 0, maka itu adalah angka FPB
 *
 * 
*/




// Cara 1
// function fpb(number1, number2) {
//    var minNumber;
//    if (number1 < number2) {
//       minNumber = number1
//    } else {
//       minNumber = number2
//    }

//    var temp = 0;
//    for (let i = 0; i < number1; i++) {
//       if (number1 % i === 0 && number2 % i === 0) {
//          temp = i;
//       }
//    }
//    return temp;
// }


// Cara 2
function fpb(number1, number2) {
   var minNumber;
   if (number1 < number2) {
      minNumber = number1;
   } else {
      minNumber = number2;
   }

   for (let i = minNumber; i >= 1; i--) {    //Perbedaan nya adalah disni menggunakan loop mundur
      if (number1 % i === 0 && number2 % i === 0) {
         return i
      }
   }
}

//Test
console.log(fpb(30, 50)) //10
console.log(fpb(12, 15)) //3
console.log(fpb(17, 35)) //1