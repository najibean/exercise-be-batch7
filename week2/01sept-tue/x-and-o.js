// 'x' and 'o'
function checkXO(string) {
    //code here
    var flagX=0;
    var flagO=0;
    for (let i = 0; i < string.length; i++) {
        if(string[i] === 'x'){
            flagX++;
        }
        else if(string[i] === 'o'){
            flagO++;
        }
    }
    if(flagX === flagO){
        return 1;
    }else {
        return -1;
    }
    // return flagX === flagO ? 1 : -1;
}

//Test
console.log(checkXO('xxxxxooooo')) //1
console.log(checkXO('xxxooooo')) //-1
console.log(checkXO('xoxoxoxoxo')) //1