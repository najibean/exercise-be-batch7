function mean(array) {
   var total = 0;
   var average;
   for (let i = 0; i < array.length; i++) {
      total = total + array[i];
   }
   average = total / array.length;

   return average.toFixed(3); // toFixed adalah untuk membulatkan angka ber-koma
}

console.log(mean([1, 2, 3, 4, 5])) //3
console.log(mean([3.7, 5.3, 7.0, 1.9, 3.1, 0.5, 1.5])) //3.28 -> 2 